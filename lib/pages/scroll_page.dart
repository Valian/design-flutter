import 'package:flutter/material.dart';

class ScrollPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        scrollDirection: Axis.vertical,
        children: <Widget>[
          _pagina1(context),
          _pagina2(),
        ],
      ),
    );
  }

  Widget _pagina1(BuildContext context) {
    return Stack(
      children: <Widget>[
        _colorFondo(),
        _imagenFondo(),
        _textos(context),
      ],
    );
  }

  Widget _pagina2() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      color: Color.fromRGBO(108, 192, 218, 1),
      child: Center(
        child: RaisedButton(
          shape: StadiumBorder(),
          color: Colors.blue,
          textColor: Colors.white,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 40, vertical: 20),
            child: Text('Bienvenidos!', style: TextStyle(fontSize: 20),),
          ),
          onPressed: (){},
        ),
      ),
    );
  }

  Widget _colorFondo() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      color: Color.fromRGBO(108, 192, 218, 1),
    );
  }

  Widget _imagenFondo() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: Image(
        image: AssetImage('assets/scroll-1.png'),
        fit: BoxFit.cover,
      ),
    );
  }

  Widget _textos(BuildContext context) {

    final stiloTexto = TextStyle(color: Colors.white, fontSize: 50);
    final pantalla = MediaQuery.of(context).size;
    return SafeArea(
          child: Column(
          children: <Widget>[
          SizedBox(height: pantalla.height * 0.1,),
          Text('11°', style: stiloTexto,),
          Text('Miércoles', style: stiloTexto,),
          Expanded(
            child: Container(),
          ),
          Icon(Icons.keyboard_arrow_down, size: 70, color: Colors.white)

        ],
      ),
    );

  }
}
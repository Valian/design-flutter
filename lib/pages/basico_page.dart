import 'package:flutter/material.dart';

class BasicoPage extends StatelessWidget {
  final estiloSubtitulo = TextStyle(fontSize: 18, color: Colors.grey);
  final estiloTitulo = TextStyle(fontSize: 20, fontWeight: FontWeight.bold);

  Widget _crearImagen (){
    return Container(
      width: double.infinity,
      child: Image(
        image: NetworkImage('https://images.pexels.com/photos/814499/pexels-photo-814499.jpeg?auto=compress'),
        height: 200,
        fit: BoxFit.cover,
      ),
    );
  }

  Widget _creartitulo(){
    return SafeArea(
        child: Container(
        padding: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text('Lago con un puente', style: estiloTitulo,),
                  SizedBox(height: 7,),
                  Text('Un lago en alemania', style: estiloSubtitulo,),
                ],
              ),
            ),
            Icon(Icons.star, color: Colors.red, size: 30,),
            Text('41', style: TextStyle(fontSize: 20),)
          ],
        ),
      ),
    );
  }

  Widget _crearAcciones(){

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        _accion(Icons.call, 'CALL'),
        _accion(Icons.near_me, 'ROUTE'),
        _accion(Icons.share, 'SHARE'),

      ],
    );

  }

  Widget _accion(IconData icon, String texto){
    return Column(
      children: <Widget>[
        Icon(icon, color: Colors.blue, size: 40,),
        SizedBox(height: 5,),
        Text(texto, style: TextStyle(fontSize: 15, color: Colors.blue),)
      ],
    );
  }

  Widget _crearTexto() {
    return SafeArea(
        child: Container(
        padding: EdgeInsets.symmetric(horizontal: 40, vertical: 20),
        child: Text('Voluptate aliqua labore anim duis in cupidatat non. Enim adipisicing tempor elit eu non laborum ullamco officia. Occaecat duis ipsum enim voluptate esse tempor aute fugiat consectetur adipisicing veniam sit ea aliquip. Esse cillum tempor sit consequat magna amet duis occaecat esse id ut aliqua in. Proident Lorem eu Lorem est consectetur magna dolor laborum. Exercitation Lorem ad eiusmod ipsum quis.',
        textAlign: TextAlign.justify,
        )
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
          _crearImagen(),
          _creartitulo(),
          _crearAcciones(),
          _crearTexto(),
          _crearTexto(),
          _crearTexto(),
          _crearTexto(),
          _crearTexto(),
        ],
        ),
      )
    );
  }
}